<?php

include_once 'donations.php';

?>
<!DOCTYPE html>
<html>
<head>

    <title>Donate</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="components/bootstrap/docs/assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="components/bootstrap/docs/assets/css/bootstrap-responsive.css"/>
    <link rel="stylesheet" href="css/donate.css"/>

</head>
<body ng-controller="DonateCtrl">
<div class="container-fluid">
    <div class="row-fluid">
        <div class="">
            <h1>Necesitamos tu apoyo</h1>

            <h2>Ayúdanos a grabar nuestro nuevo disco</h2>

            <!--<div id="myCarousel" class="carousel slide">-->
            <!--<ol class="carousel-indicators">-->
            <!--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
            <!--<li data-target="#myCarousel" data-slide-to="1"></li>-->
            <!--</ol>-->
            <!--&lt;!&ndash; Carousel items &ndash;&gt;-->
            <!--<div class="carousel-inner">-->
            <!--<div class="active item">-->
            <!--<img src="img/IMG_7128.jpg" alt=""/>-->
            <!--</div>-->
            <!--<div class="item">-->
            <!--<img src="img/IMG_6965.jpg" alt=""/>-->
            <!--</div>-->
            <!--</div>-->
            <!--&lt;!&ndash; Carousel nav &ndash;&gt;-->
            <!--<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>-->
            <!--<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>-->
            <!--</div>-->

        </div>
    </div>
    <div class="row-fluid">
        <table class="row-fluid span4">
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
            <tr>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
                <td><img src="img/logo.jpg" alt=""/></td>
            </tr>
        </table>
    </div>
    <div class="row-fluid">
        <ul class="unstyled">
            <li class="well span3" onclick="window.location.href='donation.php?amount=10' ">
                <h2><a href="donation.php?amount=10">Aporta 10 €</a></h2>
                <ul>
                    <?php foreach ($donations['10'] as $li): ?>
                        <li><?php echo $li ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="well span3" onclick="window.location.href='donation.php?amount=20' ">
                <h2><a href="donation.php?amount=20">Aporta 20 €</a></h2>
                <ul>
                    <?php foreach ($donations['20'] as $li): ?>
                        <li><?php echo $li ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="well span3" onclick="window.location.href='donation.php?amount=50' ">
                <h2><a href="donation.php?amount=50">Aporta 50 €</a></h2>
                <ul>
                    <?php foreach ($donations['50'] as $li): ?>
                        <li><?php echo $li ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="well span3" onclick="window.location.href='donation.php?amount=100' ">
                <h2><a href="donation.php?amount=100">Aporta 100 €</a></h2>
                <ul>
                    <?php foreach ($donations['100'] as $li): ?>
                        <li><?php echo $li ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="well span3" onclick="window.location.href='donation.php?amount=200' ">
                <h2><a href="donation.php?amount=200">Aporta 200 €</a></h2>
                <ul>
                    <?php foreach ($donations['200'] as $li): ?>
                        <li><?php echo $li ?></li>
                    <?php endforeach; ?>
                </ul>
            </li>
        </ul>
    </div>
</div>

<script src="components/jquery/jquery.min.js"></script>
<script type="text/javascript" src="components/bootstrap/docs/assets/js/bootstrap.min.js"></script>
</body>

</html>