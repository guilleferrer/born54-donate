<?php
const CDS = 'CD de Born54 : "Mejor Así"';
const AGRADECIMIENTOS = 'Figurarás en los Agradecimientos en la contraportada del CD';
const ENTRADAS = 'entradas para el show de lanzamiento en la Sala APOLO de Barcelona';
const REGALO_SORPPRESA = 'Regalo Sorpresa';

$donations = array(
    '10' => array('1 ' . CDS, '1 entrada para el show de lanzamiento en la Sala APOLO de Barcelona'),
    '20' => array('2 ' . CDS, '2 ' . ENTRADAS, AGRADECIMIENTOS),
    '50' => array('4 ' . CDS, '4 ' . ENTRADAS, AGRADECIMIENTOS, REGALO_SORPPRESA),
    '100' => array('6 ' . CDS, '6 ' . ENTRADAS, AGRADECIMIENTOS, REGALO_SORPPRESA),
    '200' => array('10 ' . CDS, '1 ' . ENTRADAS, AGRADECIMIENTOS)
);