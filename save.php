<?php


if (!isset($_POST['donator'])) {
    echo "error";
    throw new Exception('Wrong arguments');
}
$donator = $_POST['donator'];

?>
<!DOCTYPE html>
<html>
<head>

    <title>Donate</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="components/bootstrap/docs/assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/donate.css"/>
</head>
<body class="container">

<div class="hero-unit">

    <h1>Muchísimas gracias <?php echo $donator['name'] ?>!</h1>

    <h3>Nos encata saber que tienes interés en dar apoyo a Born54</h3>

    <h4>Haz ahora tu donación haciendo click en este botón</h4>

    <h5>Este gesto que estás apunto de hacer es una gran ayuda para poder grabar nuestro CD</h5>


    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_donations">
        <input type="hidden" name="business" value="guilleferrer@gmail.com">
        <input type="hidden" name="lc" value="ES">
        <input type="hidden" name="item_name" value="Born54">
        <input type="hidden" name="amount" value="<?php echo round($donator['amount'], 2) ?>">
        <input type="hidden" name="currency_code" value="EUR">
        <input type="hidden" name="no_note" value="0">
        <input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_LG.gif:NonHostedGuest">
        <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_LG.gif" border="0"
               name="submit" alt="PayPal. La forma rápida y segura de pagar en Internet.">
        <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1">
    </form>
</div>
</body>
</html>
