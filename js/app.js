angular.module('app', [ 'ng', 'ui.bootstrap' ]).
    controller('DonateCtrl', function ($scope) {
        $scope.donants = [
            {
                name: 'Guille'
            },
            {
                name: 'Leo'
            },
            {
                name: 'Blai'
            },
            {
                name: 'Jeroni'
            },
            {
                name: 'Juan'
            }
        ];
    });
