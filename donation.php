<?php

include_once 'donations.php';

$donateAmount = $_GET['amount'];

if (!isset($donations[$donateAmount])) {
    header('Location: /');
    exit;
}

$currentDonation = $donations[$donateAmount];
?>
<!DOCTYPE html>
<html>
<head>

    <title>Donate</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="components/bootstrap/docs/assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="css/donate.css"/>
</head>
<body>

<div id="donation" class="container-fluid">

    <!--    <div class="row-fluid">-->
    <!--        <div class="well">-->
    <!--            <img src="" alt="" class="span6"/>-->
    <!--            <div class="span6">-->
    <!--                <h1>XS</h1>-->
    <!--                <ul>-->
    <!--                    <li>1 CD de Born54</li>-->
    <!--                    <li>1 entrada para el show de lanzamiento en la Sala APOLO de Barcelona</li>-->
    <!--                </ul>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="row-fluid">

        <div class="well">
            <h2>Aporta <?php echo $donateAmount ?> €</h2>

            <h3>¡Y aplaudiremos con las orejas de ilusión!</h3>
            <h4>Y, a parte, te damos: </h4>
            <ul class="unstyled">
                <?php foreach ($currentDonation as $li) : ?>
                    <li><?php echo $li ?></li>
                <?php endforeach ?>

            </ul>
            <form action="save.php" method="POST">
                <label for="donator-name">Nombre:</label>
                <input type="text" name="donator[name]" id="donator-name" required="required" placeholder="Mi Nombre"
                       autofocus="true"/>
                <label for="donator-mail">E-mail:</label>
                <input type="email" name="donator[email]" id="donator-mail" required="required"
                       placeholder="mi@email.com"/>
                <input type="hidden" name="donator[amount]" value="10"/>
                <br>
                <input type="submit" class="btn btn-large btn-info" value="Quiero dar apoyo a Born54"/>
            </form>
        </div>
    </div>
</div>

<script src="components/jquery/jquery.min.js"></script>
<script type="text/javascript" src="components/bootstrap/docs/assets/js/bootstrap.min.js"></script>
</body>

</html>